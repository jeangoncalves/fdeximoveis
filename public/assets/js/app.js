(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function() {
	'use strict';


	var app = angular.module("myApp", ['ngRoute', 'ngStorage']);

	require('./router')(app);
	require('./controller')(app);
	require('./service')(app);
})();

},{"./controller":2,"./router":4,"./service":5}],2:[function(require,module,exports){
module.exports = function(app) {
	'use strict';

	var structure = require('./structure')();
	
	app.controller('homeCtrl', function($scope, $http, $localStorage, srvList) {
		srvList.getAll($scope, $http, $localStorage);
	});

	app.controller('itemCtrl', function($scope, $http, $localStorage, $routeParams, $location, srvList) {

		$scope.edit = ($location.path().indexOf('edit') !== -1) ? true : false;

		$scope.location = ($scope.edit) ? 'Editar' : 'Visualizar';

		if (_.isEmpty($routeParams)) {
			$scope.item = new structure.Item();
			$scope.edit = true;
			$scope.location = 'Novo';
		} else {
			srvList.getItem($scope, $http, $localStorage, $routeParams);
		}
		
		$scope.typeOptions = _.each(new structure.defaultOptions(), function(opt) { 
			if (opt.value === $scope.item.type) {
				opt.selected = true;
			}
			return opt; 
		});

		$scope.moreImage = function() {
			$scope.item.images.push(new structure.Image());
		};

		$scope.removeImage = function(img) {
			var index = _.indexOf($scope.item.images, img);
			$scope.item.images.splice(index, 1);
		};

		$scope.morePlace = function() {
			$scope.item.place.push(new structure.Place());
		};

		$scope.removePlace = function(plc) {
			var index = _.indexOf($scope.item.place, plc);
			$scope.item.place.splice(index, 1);
		};

		$scope.class = {
			image: 'is-hidden',
			place: 'is-hidden',
			edit: function() {
				return (!$scope.edit) ? 'is-disabled' : '';
			}
		};

		$scope.toggle = {
			image: function() {
				if ($scope.class.image === '') {
					$scope.class.image = 'is-hidden';
					return;
				}
				$scope.class.image = '';
			},
			place: function() {
				if ($scope.class.place === '') {
					$scope.class.place = 'is-hidden';
					return;
				}
				$scope.class.place = '';
			}
		};

		$scope.sendItem = function() {
			srvList.setItem($scope, $http, $localStorage);
		};
	});
};

},{"./structure":6}],3:[function(require,module,exports){
module.exports = function() {

	var initialize = function() {
		if (!window.Notification) {
			console.warn('Este browser não suporta Web Notifications!');
			return;
		}
		if (Notification.permission === 'default') {
			Notification.requestPermission();
		}
		return true;
	};

	var show = function(id, msg) {
		if (initialize()) {
			new Notification('Fdex imóveis', {
	            body: msg,
	            tag: 'item_' + id,
	            icon: 'https://fdex.com.br/assets/images/fx-logo-white.png'			        
	        });
		}
	};

	return function() {
		initialize();

		return {
			show: show
		};
	}();
};
},{}],4:[function(require,module,exports){
module.exports = function(app) {
	'use strict';

	app.config(function($routeProvider) {
	    $routeProvider
	    .when("/", {
	        templateUrl : 'public/view/home.html',
	        controller: 'homeCtrl'
	    })
	    .when('/view/:id', {
	    	templateUrl: 'public/view/item.html',
	    	controller: 'itemCtrl'
	    })
	    .when('/edit/:id', {
	    	templateUrl: 'public/view/item.html',
	    	controller: 'itemCtrl'
	    })
	    .when('/new', {
	    	templateUrl: 'public/view/item.html',
	    	controller: 'itemCtrl'
	    })
	    .otherwise({
	    	templateUrl: 'public/view/404.html'
	    });
	});
};

},{}],5:[function(require,module,exports){
module.exports = function(app) {
	'use strict';

	var notify = require('./notify')();

	var url = 'http://demo1183916.mockable.io/anuncio';

	var httpGet = function($http, callback) {
		$http({
			method: 'GET',
			async: false,
			url: url
		}).then(callback);
	};

	var httpPost = function($http, $scope, callback) {
		$http({
			method: 'POST',
			header: {
				'authentication': 'egeniusfounders2016'
			},
			async: false,
			data: $scope.item,
			url: url
		}).then(callback);
	};

	app.service('srvList', function() {
		this.getAll = function($scope, $http, $localStorage) {
			if ($localStorage.item === undefined) {
				httpGet($http, function(response) {
					$localStorage.item = response.data.result;
					$scope.advertising = response.data.result;
					return;
				});
			}
			$scope.advertising = $localStorage.item;
		};

		this.getItem = function($scope, $http, $localStorage, $routeParams) {
			if ($localStorage.item === undefined) {
				httpGet($http, function(response) {
					$localStorage.item = response.data.result;
				});
			}
			$scope.advertising = $localStorage.item;
			$scope.item = _.findWhere($localStorage.item, {id: Number($routeParams.id)});
		};

		this.setItem = function($scope, $http, $localStorage) {
			httpPost($http, $scope, function(data) {
				if (data.status === 200) {
					var message = 'Item Alterado';
					if ($scope.item.id === null) {
						$scope.item.id = $localStorage.item.length;
						message = 'Item Adicionado.';
						$localStorage.item.push($scope.item);
					}

					notify.show($scope.item.id, message);
					window.location.href = '#/';
				}
			});
		};
	});
};

},{"./notify":3}],6:[function(require,module,exports){
module.exports = function() {
	'use strict';

	var Item = function() {
		this.id = null;
		this.name = '';
		this.description = '';
		this.type = '';
		this.value = 0;
		this.images = [];
		this.images.push(new Image());
		this.place = [];
		this.place.push(new Place());
		this.contact = '';
	};

	var Image = function() {
		this.desc = '';
		this.url = '';
	};

	var Place = function() {
		this.address = '';
	};

	var defaultOptions = function() {
		return [
			{name: 'Casa', value: 'House', selected: false},
			{name: 'Apartamento', value: 'Appartment', selected: false},
			{name: 'Kitinete', value: 'Kitnet', selected: false}
		];
	};

	return {
		Item: Item,
		Image: Image,
		Place: Place,
		defaultOptions: defaultOptions,
	};
};
},{}]},{},[1])