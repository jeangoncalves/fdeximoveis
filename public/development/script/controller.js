module.exports = function(app) {
	'use strict';

	var structure = require('./structure')();
	
	app.controller('homeCtrl', function($scope, $http, $localStorage, srvList) {
		srvList.getAll($scope, $http, $localStorage);
	});

	app.controller('itemCtrl', function($scope, $http, $localStorage, $routeParams, $location, srvList) {

		$scope.edit = ($location.path().indexOf('edit') !== -1) ? true : false;

		$scope.location = ($scope.edit) ? 'Editar' : 'Visualizar';

		if (_.isEmpty($routeParams)) {
			$scope.item = new structure.Item();
			$scope.edit = true;
			$scope.location = 'Novo';
		} else {
			srvList.getItem($scope, $http, $localStorage, $routeParams);
		}
		
		$scope.typeOptions = _.each(new structure.defaultOptions(), function(opt) { 
			if (opt.value === $scope.item.type) {
				opt.selected = true;
			}
			return opt; 
		});

		$scope.moreImage = function() {
			$scope.item.images.push(new structure.Image());
		};

		$scope.removeImage = function(img) {
			var index = _.indexOf($scope.item.images, img);
			$scope.item.images.splice(index, 1);
		};

		$scope.morePlace = function() {
			$scope.item.place.push(new structure.Place());
		};

		$scope.removePlace = function(plc) {
			var index = _.indexOf($scope.item.place, plc);
			$scope.item.place.splice(index, 1);
		};

		$scope.class = {
			image: 'is-hidden',
			place: 'is-hidden',
			edit: function() {
				return (!$scope.edit) ? 'is-disabled' : '';
			}
		};

		$scope.toggle = {
			image: function() {
				if ($scope.class.image === '') {
					$scope.class.image = 'is-hidden';
					return;
				}
				$scope.class.image = '';
			},
			place: function() {
				if ($scope.class.place === '') {
					$scope.class.place = 'is-hidden';
					return;
				}
				$scope.class.place = '';
			}
		};

		$scope.sendItem = function() {
			srvList.setItem($scope, $http, $localStorage);
		};
	});
};
