module.exports = function(app) {
	'use strict';

	var notify = require('./notify')();

	var url = 'http://demo1183916.mockable.io/anuncio';

	var httpGet = function($http, callback) {
		$http({
			method: 'GET',
			async: false,
			url: url
		}).then(callback);
	};

	var httpPost = function($http, $scope, callback) {
		$http({
			method: 'POST',
			header: {
				'authentication': 'egeniusfounders2016'
			},
			async: false,
			data: $scope.item,
			url: url
		}).then(callback);
	};

	app.service('srvList', function() {
		this.getAll = function($scope, $http, $localStorage) {
			if ($localStorage.item === undefined) {
				httpGet($http, function(response) {
					$localStorage.item = response.data.result;
					$scope.advertising = response.data.result;
					return;
				});
			}
			$scope.advertising = $localStorage.item;
		};

		this.getItem = function($scope, $http, $localStorage, $routeParams) {
			if ($localStorage.item === undefined) {
				httpGet($http, function(response) {
					$localStorage.item = response.data.result;
				});
			}
			$scope.advertising = $localStorage.item;
			$scope.item = _.findWhere($localStorage.item, {id: Number($routeParams.id)});
		};

		this.setItem = function($scope, $http, $localStorage) {
			httpPost($http, $scope, function(data) {
				if (data.status === 200) {
					var message = 'Item Alterado';
					if ($scope.item.id === null) {
						$scope.item.id = $localStorage.item.length;
						message = 'Item Adicionado.';
						$localStorage.item.push($scope.item);
					}

					notify.show($scope.item.id, message);
					window.location.href = '#/';
				}
			});
		};
	});
};
