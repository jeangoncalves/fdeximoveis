(function() {
	'use strict';


	var app = angular.module("myApp", ['ngRoute', 'ngStorage']);

	require('./router')(app);
	require('./controller')(app);
	require('./service')(app);
})();
