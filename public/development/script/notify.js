module.exports = function() {

	var initialize = function() {
		if (!window.Notification) {
			console.warn('Este browser não suporta Web Notifications!');
			return;
		}
		if (Notification.permission === 'default') {
			Notification.requestPermission();
		}
		return true;
	};

	var show = function(id, msg) {
		if (initialize()) {
			new Notification('Fdex imóveis', {
	            body: msg,
	            tag: 'item_' + id,
	            icon: 'https://fdex.com.br/assets/images/fx-logo-white.png'			        
	        });
		}
	};

	return function() {
		initialize();

		return {
			show: show
		};
	}();
};