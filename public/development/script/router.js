module.exports = function(app) {
	'use strict';

	app.config(function($routeProvider) {
	    $routeProvider
	    .when("/", {
	        templateUrl : 'public/view/home.html',
	        controller: 'homeCtrl'
	    })
	    .when('/view/:id', {
	    	templateUrl: 'public/view/item.html',
	    	controller: 'itemCtrl'
	    })
	    .when('/edit/:id', {
	    	templateUrl: 'public/view/item.html',
	    	controller: 'itemCtrl'
	    })
	    .when('/new', {
	    	templateUrl: 'public/view/item.html',
	    	controller: 'itemCtrl'
	    })
	    .otherwise({
	    	templateUrl: 'public/view/404.html'
	    });
	});
};
