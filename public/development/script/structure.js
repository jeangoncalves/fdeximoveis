module.exports = function() {
	'use strict';

	var Item = function() {
		this.id = null;
		this.name = '';
		this.description = '';
		this.type = '';
		this.value = 0;
		this.images = [];
		this.images.push(new Image());
		this.place = [];
		this.place.push(new Place());
		this.contact = '';
	};

	var Image = function() {
		this.desc = '';
		this.url = '';
	};

	var Place = function() {
		this.address = '';
	};

	var defaultOptions = function() {
		return [
			{name: 'Casa', value: 'House', selected: false},
			{name: 'Apartamento', value: 'Appartment', selected: false},
			{name: 'Kitinete', value: 'Kitnet', selected: false}
		];
	};

	return {
		Item: Item,
		Image: Image,
		Place: Place,
		defaultOptions: defaultOptions,
	};
};