var gulp = require('gulp'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    browserify = require('browserify'),
    replace = require('gulp-replace'),
    plumber = require('gulp-plumber'),
    notify = require('gulp-notify'),
    minify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    gutil = require('gulp-util'),
    allFiles = ['public/development/script/**/*.js'],
    sassSources = ['public/development/sass/**/*.scss'];

gulp.task('jsQuality', function() {
    var lint = require('gulp-jshint');

    gulp.src(allFiles)  
        .pipe(lint())
        .pipe(notify(function(file) {
            if (file.jshint.success) {
                return false;
            }

            var errors = file.jshint.results.map(function(data) {
                if (data.error) {
                    return '(' + data.error.line + ':' + data.error.character + ') ' + data.error.reason;
                }
            }).join('\n');

            return file.relative + '\n' + errors;
        }));
});

gulp.task('jsBundle', function() {
    var file = browserify({
        entries: './public/development/script/app.js',
        debug: true,
    });

    return file.bundle()
        .on('error', function(err) {
            return notify().write(err);
        })
        .pipe(plumber({
            errorHandler: notify.onError('Erro no build do app.js.')
        }))
        .pipe(source('app.js'))
        .pipe(buffer())
        .pipe(gulp.dest('public/assets/js'));
});

gulp.task('jsDeploy', function() {
    var file = browserify({
        entries: './public/development/script/app.js',
        debug: false
    });

    return file.bundle()
        .on('error', function(err) {
            return notify().write(err);
        })
        .pipe(plumber({
            errorHandler: notify.onError('Erro no build do app.js.')
        }))
        .pipe(source('app.js'))
        .pipe(buffer())
        .pipe(replace('\'use strict\';', ''))
        .pipe(gulp.dest('public/assets/js'));
});

// Task to compile SASS files
gulp.task('sass', function() {  
    gulp.src(sassSources)
        .pipe(sass({
            outputStyle: 'compressed' 
        })
        .on('error', gutil.log)) 
        .pipe(gulp.dest('public/assets/css')); 
});

gulp.task('default', ['js', 'sass']);
gulp.task('js', ['jsQuality', 'jsBundle']);
gulp.task('deploy', ['jsDeploy']);

gulp.task('js:watch', function() {
    gulp.run('js');
    gulp.watch(allFiles, ['js']);
});
gulp.task('sass:watch', function() { 
    gulp.watch(sassSources,['sass']);
});