# FdexImóveis

Primeira aplicação utilizando AngularJS.

Aplicação para controle de imóveis com inserção de informações, listagem dos imóveis e visualização de cada um.

## Workflow

Para um ambiente de trabalho com escalabilidade, foi utilizado o `gulp` para trabalhar com arquivos modularizados.

## Estrutura

``` sh
+-- public
|   +-- assets
|   |   +-- css
|   |   |   +-- style.css		// Arquivo principal de CSS
|   |   +-- js
|   |   |   +-- app.js 			// Arquivo principal de JS
|   |
|   +-- development
|   |   +-- sass
|   |   |   +-- style.scss 		// Arquivo desenvolvimento de SCSS
|   |
|   |   +-- script
|   |   |   +-- app.js 			// Arquivo desenvolvimento de JS (aqui que carrega os módulos)
|   |   |   +-- controller.js 	// Módulo controller do app
|   |   |   +-- notify.js 		// Arquivo de Notificação nativa do navegador
|   |   |   +-- router.js 		// Módulo de rotas do app
|   |   |   +-- service.js 		// Módulo de serviços do app
|   |   |   +-- structure.js 	// Arquivo de estruturas de dados
|   |
|   +-- view
+-- package.json
+-- gulpfiles.js				// Arquivo de configuração das tarefas automatizadas do gulp
```


## Gulp

Para rodar as tarefas do gulp, acesse o terminal na pasta raiz do projeto

Ex.:
``` sh
$ www/fdeximoveis
```

As tarefas disponíveis são:

``` sh
gulp js 			// Verifica erros e concordância e agrupa os módulos dentro de development/script

gulp js:watch 		// O mesmo de cima, porém com watch	

gulp sass 			// Verifica erros e concordância, minifica e agrupa os módulos dentro de development/sass

gulp sass:watch 	// O mesmo de cima, porém com watch	
```

*A intenção era de colocar nas tarefas `js` a minificação, porém ocorreu uma falha ao carregar a aplicação com o arquivo minificado.*


## SASS / Layout

Por ter de dedicar certo tempo para o AngularJS que não era muito habituado, o Layout eu tive que inserir um Framework, até mesmo por que não tinha nenhum Layout definido, e isso me custaria um tempo maior do que o necessário para desenvolver a aplicação.

Foi utilizado o Framework [Bulma](http://bulma.io) que deixou a aplicação com uma aparência muito boa para dispositivos móveis, mas faltou ajustar para desktop.


## Curva de aprendizado

Independente de toda a dificuldade, o aprendizado com AngularJS foi muito positivo e de relevância para meu conhecimento.

O workflow do Gulp também não tinha, tive de dedicar certo tempo para criar um com algumas funcionalidades interessantes para o desenvolvimento.

Gostei também de trabalhar com o [mockup.io](https://mockup.io/) pois nunca tinha visto esse serviço, e é bem interessante para o desenvolvimento front-end.